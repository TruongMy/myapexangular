import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StaticAPIComponent } from './static-api/static-api.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { EditFetchDataComponent } from './edit-fetch-data/edit-fetch-data.component';
import { HotTableModule } from '@handsontable/angular';
@NgModule({
  declarations: [
    AppComponent,
    StaticAPIComponent,
    NavMenuComponent,
    HomeComponent,
    FetchDataComponent,
    EditFetchDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HotTableModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
