import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticAPIComponent } from './static-api.component';

describe('StaticAPIComponent', () => {
  let component: StaticAPIComponent;
  let fixture: ComponentFixture<StaticAPIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaticAPIComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StaticAPIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
