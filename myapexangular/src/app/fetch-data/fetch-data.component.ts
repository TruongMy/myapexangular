import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  styleUrls: ['./fetch-data.component.scss']
})
export class FetchDataComponentm implements OnInit {
  public forecasts?: WeatherForecast[];
  ngOnInit(): void {

  }
  constructor(http: HttpClient) {
    http.get<WeatherForecast[]>(environment.baseUrl + 'assets/static-api/weatherforecast.json').subscribe(result => {
      this.forecasts = result;
    }, error => console.error(error));
  }


}
interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}
