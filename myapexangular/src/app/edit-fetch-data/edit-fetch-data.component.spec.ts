import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFetchDataComponent } from './edit-fetch-data.component';

describe('EditFetchDataComponent', () => {
  let component: EditFetchDataComponent;
  let fixture: ComponentFixture<EditFetchDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFetchDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditFetchDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
